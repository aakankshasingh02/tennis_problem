class TennisGame:
    def __init__(self, player1_name, player2_name):
        self.player1_name = player1_name
        self.player2_name = player2_name
        self.player1_score = 0
        self.player2_score = 0

    def won_point(self, player_name):
        if player_name == self.player1_name:
            self.player1_score += 1
        elif player_name == self.player2_name:
            self.player2_score += 1

    def get_winner(self):
        if self.player1_score >= 4 and self.player1_score - self.player2_score >= 2:
            return self.player1_name
        elif self.player2_score >= 4 and self.player2_score - self.player1_score >= 2:
            return self.player2_name
        else:
            return None


class TennisSet:
    def __init__(self, player1_name, player2_name):
        self.player1_name = player1_name
        self.player2_name = player2_name
        self.player1_games = 0
        self.player2_games = 0

    def won_game(self, player_name):
        if player_name == self.player1_name:
            self.player1_games += 1
        elif player_name == self.player2_name:
            self.player2_games += 1

    def get_winner(self):
        if self.player1_games >= 6 and self.player1_games - self.player2_games >= 2:
            return self.player1_name
        elif self.player2_games >= 6 and self.player2_games - self.player2_name >= 2:
            return self.player2_name
        else:
            return None


class TennisMatch:
    def __init__(self, player1_name, player2_name):
        self.player1_name = player1_name
        self.player2_name = player2_name
        self.player1_sets = 0
        self.player2_sets = 0
        self.current_set = TennisSet(player1_name, player2_name)
        self.current_game = TennisGame(player1_name, player2_name)

    def won_point(self, player_name):
        winner = self.current_game.get_winner()
        if winner:
            self.current_set.won_game(winner)
            self.current_game = TennisGame(self.player1_name, self.player2_name)
        else:
            self.current_game.won_point(player_name)

        winner = self.current_set.get_winner()
        if winner:
            if winner == self.player1_name:
                self.player1_sets += 1
            else:
                self.player2_sets += 1
            self.current_set = TennisSet(self.player1_name, self.player2_name)
            self.current_game = TennisGame(self.player1_name, self.player2_name)

    def get_winner(self):
        if self.player1_sets >= 2:
            return self.player1_name
        elif self.player2_sets >= 2:
            return self.player2_name
        else:
            return None


def main():
    player1_name = "Player A"
    player2_name = "Player B"
    match = TennisMatch(player1_name, player2_name)

    input_string = "AAAABABABABABABBBAAAA"
    for char in input_string:
        if char == 'A':
            match.won_point(player1_name)
        elif char == 'B':
            match.won_point(player2_name)

    winner = match.get_winner()
    if winner:
        print(f"The winner is {winner}!")
    else:
        print("Match not finished")


if __name__ == "__main__":
    main()